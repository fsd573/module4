import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent implements OnInit {
  emailId: any;
  // products: any;
  // localStorageData: any;

  cartItems: any;
  cartProducts: any;

  // constructor() {
  //   this.emailId = localStorage.getItem('emailId');
  //   this.localStorageData = localStorage.getItem('cartItems');
  //   this.products = JSON.parse(this.localStorageData);
  //   console.log(this.products);
  // }

  constructor(private service: EmpService) {
    this.cartItems = service.getCartItems();
  }

  ngOnInit() {
  }
  // onPurchase() {
    
  //   localStorage.removeItem('cartItems');
  //   this.cartItems = [];
    
  // }
  onDelete(index: number) {
    this.cartItems.splice(index, 1);
    this.calculateTotal();
  }

  calculateTotal(): number {
    // Calculate the total based on product prices
    return this.cartItems.reduce((total:any, product:any) => total + (product.price || 0), 0);
  }


}