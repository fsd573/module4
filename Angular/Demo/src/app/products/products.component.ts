import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: any;
  emailId: any;
  cartItems: any;
  selectedProduct: any;

  constructor(private router: Router, private service: EmpService) {
    this.emailId = localStorage.getItem('emailId');
    this.cartItems = [];
    
    this.products = [
       {pName:"Laptop",   price:56000.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/Laptop.jpg"},
       {pName:"Tab",      price:34000.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/Tab.jpg"},
       {pName:"IPhone",   price:69999.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/IPhone.jpeg"},
       {pName:"IWatch",   price:42999.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/iWatch.jpg"},
       {pName:"Airpodes", price:10999.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/Airpods.jpg"},
       {pName:"Camera",   price:49999.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/Camera.jpg"}
    ];
  }

  ngOnInit() {    
  }
  addToCart(product: any) {
    this.service.addToCart(product);

    // this.cartProducts.push(product);
    // localStorage.setItem("cartItems", JSON.stringify(this.cartProducts));
  }

  // selectProduct(product: any) {
  //   this.selectedProduct = product;
  // }

  onClickProduct(product: any): void {
    localStorage.setItem('selectedProduct', JSON.stringify(product));
    this.router.navigate(['productinfo']);
  }
  
}