import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gender'
})
export class GenderPipe implements PipeTransform {

  gender: any;
  name: any;

  transform(name: any, gender: any): any {
    if (gender == 'Male'){
      return 'MR.' + name; 
    } else if (gender == 'Female'){
      return 'MISS.' + name;
    } 
    return name;
  }

}
