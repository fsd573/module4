import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {

  emailId: any;
  password: any;
  employees: any;
  emp: any;
  captchaResponse: string = '';


  constructor(private router: Router, private toastr: ToastrService, private service: EmpService) {
    this.employees = {
      empName:'',
      salary:'',
      gender:'',
      doj:'',
      country:'',
      emailId:'',
      password:'',
      phonenumber:'',

    };
      
    
  }

  ngOnInit() {
  }

  submit(regForm: any) {
    console.log("EmailId : " + this.emailId);
    console.log("Password: " + this.password);
    console.log(regForm);

  
  if (!this.captchaResponse) {
    alert('Please complete the captcha.');
    return;
  }
}

async loginSubmit(loginForm: any) {
  if (loginForm.emailId == 'HR' && loginForm.password == 'HR') {           
    //Setting the isUserLoggedIn variable value to true under EmpService
    this.service.setIsUserLoggedIn();
    localStorage.setItem("emailId", loginForm.emailId);
    this.router.navigate(['showemps']);
  } else {
    this.emp = null;

    await this.service.employeeLogin(loginForm.emailId, loginForm.password).then((data: any) => {
      console.log(data);
      this.emp = data;
    });

    if (this.emp != null) {
      this.service.setIsUserLoggedIn();        
      localStorage.setItem("emailId", loginForm.emailId);
      this.router.navigate(['products']);
    } else {
      alert('Invalid Credentials');
    }
  }
  }
  handleCaptchaResponse(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }
}
