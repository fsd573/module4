// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { ToastrService } from 'ngx-toastr';
// import { EmpService } from '../emp.service';

// @Component({
//   selector: 'app-register',
//   templateUrl: './register.component.html',
//   styleUrls: ['./register.component.css']
// })
// export class RegisterComponent implements OnInit {
  
  
//   empName: any;
//   salary: any;
//   gender: any;
//   doj: any;
//   country: any;
//   phoneNumber: any;
//   emailId: any;
//   password: any;
//   countries: any;

//   departments: any;

//   constructor(private router: Router, private toastr: ToastrService,private service: EmpService) {
//   }

//   ngOnInit() {
//     this.service.getAllCountries().subscribe((data: any) => {
//       this.countries = data;
//       this.service.getAllDepartments().subscribe((data: any) => {this.departments = data;});
//       console.log(data);
//     });
//   }

//   submit() {
//     console.log("EmpName: " + this.empName);
//     console.log("Salary: " + this.salary);
//     console.log("Gender: " + this.gender);
//     console.log("DateOfJoin: " + this.doj);
//     console.log("Country: " + this.country);
//     console.log("PhoneNumber: " + this.phoneNumber);
//     console.log("Email-Id: " + this.emailId);
//     console.log("Password: " + this.password);
//   }

//   registerSubmit(regForm: any) {
//     console.log(regForm);

//     if (!this.validateForm()) {
//       return;
//     }

//     const newEmployee = {
//       empId: this.generateEmpId(),
//       empName: this.empName,
//       salary: this.salary,
//       gender: this.gender,
//       doj: this.doj,
//       country: this.country,
//       phoneNumber: this.phoneNumber,
//       emailId: this.emailId,
//       password: this.password
//     };

//     console.log(newEmployee);

//     this.toastr.success('Registration Successful!', 'Success', {
//       closeButton: true,
//       progressBar: true,
//       positionClass: 'toast-top-right',
//       tapToDismiss: false,
//       timeOut: 3000, // 3 seconds
//     });

//     this.router.navigate(['login']);
//   }

//   private validateForm(): boolean {

//     if (!this.empName || !this.salary || !this.emailId ) {
//       this.toastr.error('Please fill in all required fields.', 'Error', {
//         closeButton: true,
//         progressBar: true,
//         positionClass: 'toast-top-right',
//         tapToDismiss: false,
//         timeOut: 2000, // 2 seconds
//       });
//       return false;
//     }
//     return true;
//   }

//   private generateEmpId(): number {
//     return Math.floor(Math.random() * 1000) + 1;
//   }
// }


  
// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { ToastrService } from 'ngx-toastr';
// import { EmpService } from '../emp.service';

// @Component({
//   selector: 'app-register',
//   templateUrl: './register.component.html',
//   styleUrls: ['./register.component.css']
// })
// export class RegisterComponent implements OnInit {

//   empName: any;
//   salary: any;
//   gender: any;
//   doj: any;
//   country: any;
//   phoneNumber: any;
//   emailId: any;
//   password: any;
//   countries: any;
//   departments: any;
//   emp: any;

//   constructor(private router: Router, private toastr: ToastrService,private service: EmpService) {
//     this.emp = {
//       empName:'',
//       salary:'',
//       gender:'',
//       doj:'',
//       country:'',
//       emailId:'',
//       password:'',
//       department: {
//         deptId:''
//       }
//     };
//   }

//   ngOnInit() {
//     this.service.getAllCountries().subscribe((data: any) => {this.countries = data;
//     this.service.getAllDepartments().subscribe((data: any) => {this.departments = data;});
//       console.log(data);
//     });
//   }

//   submit() {
//     console.log("EmpName: " + this.empName);
//     console.log("Salary: " + this.salary);
//     console.log("Gender: " + this.gender);
//     console.log("DateOfJoin: " + this.doj);
//     console.log("Country: " + this.country);
//     console.log("Departments: " + this.departments);
//     console.log("PhoneNumber: " + this.phoneNumber);
//     console.log("Email-Id: " + this.emailId);
//     console.log("Password: " + this.password);
//   }

//   registerSubmit(registrationForm: any) {
//     console.log(registrationForm);

    
//     if (!this.validateForm()) {
//       return;
//     }
//     const newEmployee = {
//       empId: this.generateEmpId(),
//       empName: this.empName,
//       salary: this.salary,
//       gender: this.gender,
//       doj: this.doj,
//       country: this.country,
//       phoneNumber: this.phoneNumber,
//       emailId: this.emailId,
//       password: this.password,
//       department: this.departments
//     };

//     console.log(newEmployee);

//     this.toastr.success('Registration Successful!', 'Success', {
//       closeButton: true,
//       progressBar: true,
//       positionClass: 'toast-top-right',
//       tapToDismiss: false,
//       timeOut: 3000, // 3 seconds
//     });

//      this.router.navigate(['login']);
//   }

//   private validateForm(): boolean {

//     if (!this.empName || !this.salary || !this.emailId) {
//       this.toastr.error('Please fill in all required fields.', 'Error', {
//         closeButton: true,
//         progressBar: true,
//         positionClass: 'toast-top-right',
//         tapToDismiss: false,
//         timeOut: 2000, // 2 seconds
//       });
//       return false;
//     }
//     return true;
//   }

//   private generateEmpId(): number {
//     return Math.floor(Math.random() * 1000) + 1;
//   }
// }





import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EmpService } from '../emp.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  countries: any;
  departments: any;
  emp: any;
  captchaResponse: string = '';
  password:string='';
  confirmPassword: string = '';
  emailId:string ='';
  mobileNumber:string='';
  doj:string='';
  gender:string='';
  salary:string='';
  empName:string='';

  constructor(private router: Router, private service: EmpService, private toastr: ToastrService) {
    this.emp = {
      empName:'',
      salary:'',
      gender:'',
      doj:'',
      country:'',
      mobileNumber:'',
      emailId:'',
      password:'',
      department: {
        deptId:''
      }
    };
  }

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => {this.countries = data;});
    this.service.getAllDepartments().subscribe((data: any) => {this.departments = data;});
  }
 

  registerSubmit(regForm: any) {
    console.log('Register form submitted:', regForm);

    const trimmedPassword = this.emp.password.trim();  
    const trimmedConfirmPassword = this.confirmPassword.trim();  

    console.log('Passwords (trimmed):', this.emp.password, this.confirmPassword);  
    console.log('Password lengths:', this.emp.password.length, this.confirmPassword.length);  

    if (this.emp.password.length > 0 && this.emp.password === this.confirmPassword) {  
        // Rest of your code remains unchanged
    } else {
        // Passwords do not match
        this.toastr.error('Passwords do not match.', 'Error', {
            closeButton: true,
            progressBar: true,
            positionClass: 'toast-top-right',
            tapToDismiss: false,
            timeOut: 3000, // 3 seconds
        });
    }
    this.emp.empName = regForm.empName;
    this.emp.salary = regForm.salary;
    this.emp.gender = regForm.gender;
    this.emp.doj = regForm.doj;
    this.emp.country = regForm.country;
    this.emp.mobileNumber = regForm.mobileNumber;
    this.emp.emailId = regForm.emailId;
    this.emp.password = regForm.password;
    this.emp.department.deptId = regForm.department;

    // console.log(this.emp);

    this.service.regsiterEmployee(this.emp).subscribe((data: any) => {console.log(data);});

    // if (!this.validateForm(regForm)) {
    //   return;
    // }
    
    this.toastr.success('Registration Successful!', 'Success', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 3000, // 3 seconds
    });

    this.router.navigate(['login']);
  }

  private validateForm(regForm: any): boolean {

    if (!regForm.empName || !regForm.salary || !regForm.emailId || !regForm.password) {
      this.toastr.error('Please fill in all required fields.', 'Error', {
        closeButton: true,
        progressBar: true,
        positionClass: 'toast-top-right',
        tapToDismiss: false,
        timeOut: 3000, // 3 seconds
      });
      return false;
    }
    return true;
  }
  private passwordsMatch(password1: string, password2: string): boolean {
    const trimmedPassword1 = password1.trim();
    const trimmedPassword2 = password2.trim();

    return trimmedPassword1 === trimmedPassword2;
}
  handleCaptchaResponse(captchaResponse: string) {
    this.captchaResponse = captchaResponse;
  }
}