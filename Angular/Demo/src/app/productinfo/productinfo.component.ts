import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-productinfo',
  templateUrl: './productinfo.component.html',
  styleUrl: './productinfo.component.css'
})
export class ProductinfoComponent  implements OnInit {
  selectedProduct: any;
  products: any;
  emailId: any;
  cartProducts: any;

  ngOnInit() {
    // Retrieve the stored product details from localStorage
    const storedProduct = localStorage.getItem('selectedProduct');

    // Parse the JSON string to get the actual object
    this.selectedProduct = storedProduct ? JSON.parse(storedProduct) : null;
    console.log(storedProduct);
  }
  addToCart(selectedProduct: any) {
    this.cartProducts = this.cartProducts || [];
    console.log('Adding to cart:', selectedProduct);

    this.cartProducts.push(selectedProduct);
    localStorage.setItem("cartItems", JSON.stringify(this.cartProducts));
    console.log('Cart after adding:', this.cartProducts);
}
}
