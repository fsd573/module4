package com.dao;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Employee;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class EmployeeDao {

//    private static final String String = null;
	
	@Autowired
	private JavaMailSender javaMailSender;
	
	 @Autowired
    EmployeeRepository employeeRepository;
	 private String generateRandomOTP() {
	        // Define the length of the OTP
	        int otpLength = 6;

	        // Characters to be used in the OTP
	        String numbers = "0123456789";

	        // Use StringBuilder to efficiently build the OTP
	        StringBuilder otp = new StringBuilder(otpLength);

	        // Generate random OTP using characters from 'numbers'
	        Random random = new Random();
	        for (int i = 0; i < otpLength; i++) {
	            int index = random.nextInt(numbers.length());
	            otp.append(numbers.charAt(index));
	        }

	        return otp.toString();
	    }
	 
	private final String twilioAccountSid = "ACcd1386df59ad4ecee9b53f5584bf9b4a";
	private final String twilioAuthToken = "18fe1bfb3f22cfb6e6b8eb3a7f0f6a8c";
	private final String twilioPhoneNumber = "+15102983579";

	static {
	     Twilio.init("ACcd1386df59ad4ecee9b53f5584bf9b4a", "18fe1bfb3f22cfb6e6b8eb3a7f0f6a8c");
	}

    public List<Employee> getEmployees() {
        return employeeRepository.findAll();
    }

    public Employee getEmployeeById(int employeeId) {
        return employeeRepository.findById(employeeId).orElse(null);
    }

    public Employee getEmployeeByName(String employeeName) {
        return employeeRepository.findByName(employeeName);
    }

    public Employee employeeLogin(String emailId, String password) {
        Employee employee = employeeRepository.findByEmailId(emailId);
        
        if (employee != null) {
            BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
            if (bcrypt.matches(password, employee.getPassword())) {
                return employee;
            }
        }
        
        return null;
    }

    public Employee addEmployee(Employee employee) {
        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        String encryptedPwd = bcrypt.encode(employee.getPassword());
        employee.setPassword(encryptedPwd);
//        return employeeRepository.save(employee);
//	}
//	
//	//Fetching details by converting password to encrypted password
//	public Employee employeeLogin1(String emailId, String password) {
//		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
//		String encodedPassword = employeeRepository.findByEmailId(emailId).getPassword();
//		if(bcrypt.matches(password, encodedPassword)){
//			return employeeRepository.findByEmailId(emailId);
//		}
//		return null;
//	}

        employee = employeeRepository.save(employee);

        sendThankYouEmail(employee.getEmailId());
        sendThankYouSMS(employee.getMobileNumber());

        return employee;
    }

    private void sendThankYouSMS(String mobileNumber) {
    	try {
            Message message = Message.creator(
                    new PhoneNumber(mobileNumber),
                    new PhoneNumber(twilioPhoneNumber),
                    generateRandomOTP()
            ).create();
            System.out.println("SMS sent successfully. SID: " + message.getSid());
        } catch (Exception e) {
            System.err.println("Error sending SMS: " + e.getMessage());
            e.printStackTrace();
        }
    }
  
//    	Message message = Message.creator(
//                new PhoneNumber(mobileNumber),
//                new PhoneNumber(twilioPhoneNumber),
////                "Thank you for registering with our application."
//                "579064"
//        ).create();
//	}
    	

	private void sendThankYouEmail(String emailId) {
		SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(emailId);
        message.setSubject("Thank You for Registering");
        message.setText("Dear User,\n\nThank you for registering with our application.");

        javaMailSender.send(message);
		
	}

	public Employee updateEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    public void deleteEmployeeById(int employeeId) {
        employeeRepository.deleteById(employeeId);
    }
}